from django import forms
from appli.models import Tag



class RechercheExerciceForm(forms.Form):
    nom_exercice = forms.CharField(widget=forms.TextInput(attrs={'class':'nom_exercice'}),max_length=200,required=False)
    select_tags  = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'class':'select_tags'}),required=False)
    
class CreerExerciceForm(forms.Form):
	nom_exercice = forms.CharField(max_length=1000)
	nouveaux_tags = forms.CharField(max_length=1000, required=False)
	enonce = forms.CharField(max_length=1000)
	codes = forms.CharField(widget=forms.Textarea(attrs={'class':'code'}))
	
class ProgramForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea(attrs={'class':'code'}))
    #~ utilisateur=formsharfield
class ConnexionForm(forms.Form):
    username = forms.CharField(label="Username", max_length=200)
    password = forms.CharField(label="Mot de Passe", widget=forms.PasswordInput)

class AffinerGraphique(forms.Form):
	nom_exo = forms.CharField(max_length=200)
	categorie = forms.CharField(max_length=200)
	
class AffinerGraphiqueProf(forms.Form):
	nom_exo = forms.CharField(max_length=200)
	categorie = forms.CharField(max_length=200)
	groupe = forms.CharField(max_length=200)

class CreerAideForm(forms.Form):
	nom_aide = forms.CharField(max_length=200, required=True)
	describ = forms.CharField(max_length=1000, required=True)

class CreerParcours(forms.Form):
	nom_parcours = forms.CharField(max_length=200, required=True)
