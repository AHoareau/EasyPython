import json
class resultats:
    def __init__(self):
        self._invalide=None
        self._mauvais_resultats=[]
        self._temps=None
    def invalide(self,raison):
        self._invalide=raison
    def temps(self,t):
        self.temps=t
    def add_mauvaises_sorties(self,sorties):
        self._mauvais_resultats.append(sorties)
    def __repr__(self):
        if self._invalide:
            return str(self._invalide)
        return str(self._mauvais_resultats)
    def dumps(self):
        return json.dumps(self.__dict__)
    def loads(chaine):
        d=json.loads(chaine)
        self._invalide=d["_invalide"]
        self._mauvais_resultats=d["_mauvais_resultats"]
        self._temps=d["_temps"]

