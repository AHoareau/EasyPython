#!/usr/bin/python3 
from . import test_fonction
from .resultats import resultats
import json,sys,os,inspect
import time

class ExercicePython:
    def charger_module(self,nom_module):
        try:
            self.module_ens=test_fonction.ouvre_module(nom_module)
            return True
        except test_fonction.Erreur as e:
            self.messages.append( (str(e), "erreur") )
            return False

    def parser_module(self):
        solution = [ (nom,fun) for nom,fun in self.module_ens.__dict__.items() if "solution" in dir(fun) ]
        if solution:
            (self.nom_solution,self.solution)=solution[0]
        self.arguments=inspect.getargspec(self.solution).args 
        self.entrees_visibles=self.module_ens.__dict__.get("entrees_visibles",[])
        self.entrees_invisibles=self.module_ens.__dict__.get("entrees_invisibles",[])
        if all(not isinstance(i,tuple) for i in self.entrees_visibles):
            self.entrees_visibles=[(x,) for x in self.entrees_visibles]
        if all(not isinstance(i,tuple) for i in self.entrees_invisibles):
            self.entrees_invisibles=[(x,) for x in self.entrees_invisibles]

        if(solution and (self.entrees_visibles or self.entrees_invisibles)):
            self.messages.append( ("Solutions et entrées, tout y est !", "bien") )
        else:
            if(self.entrees_visibles or self.entrees_invisibles):
                self.messages.append( ("Il y a des entrées mais pas de quoi tester","pasbien") )
                return False
            elif solution:
                self.messages.append( ("Il faut des entrées", "pasbien") )
                return False
            else:
                self.messages.append( ("Il faut des entrées et de quoi tester", "pasbien") )
                return False
        if(self.entrees_visibles and not self.entrees_invisibles):
            self.messages.append( ("Toutes les entrées sont visibles!","info") )
        if(self.entrees_invisibles and not self.entrees_visibles):
            self.messages.append( ("Toutes les entrées sont invisibles!", "info") )
        return True


    def tester_solution_ens(self):
        try:
            time0=time.time()
            self.solutions_visibles=test_fonction.test_fonction(self.solution,self.entrees_visibles)
            self.solutions_invisibles=test_fonction.test_fonction(self.solution,self.entrees_invisibles)
            temps=time.time()-time0
            self.temps=temps
            return True
        except test_fonction.Erreur as e:
            self.messages.append( (str(e),"erreur") )
            return False
    def afficher(self):
        attrs=["messages","entrees_visibles","entrees_invisibles","solutions_visibles","solutions_invisibles","temps"]
        res={attr:self.__dict__[attr] for attr in attrs if self.__dict__[attr] }
        print(json.dumps(res))
        
    def __init__(self,module):
        self.messages=[]
        self.module_ens=None
        self.entrees_visibles=[]
        self.entrees_invisibles=[]
        self.solutions_visibles=[]
        self.solutions_invisibles=[]
        self.temps=None
        self.solution=None
        self.nom_solution=None
        self.arguments=None
        self.module_charge=False
        if (self.charger_module(module) and self.parser_module() and self.tester_solution_ens()):
            self.module_charge=True

    def tester_solution_etu(self,nom_module_etu):
        resultat=resultats()
        if not self.module_charge:
            resultat.invalide({"erreur":"probleme dans l'exercice"})
            return resultat
        try:
            module_etu=test_fonction.ouvre_module(nom_module_etu)
        except test_fonction.Erreur as e:
            resultat.invalide({"exception":e.exception, "erreur":e.erreur})
            return resultat

        if not self.nom_solution in dir(module_etu):
            resultat.invalide({"erreur":"Votre programme doit contenir une fonction "+self.nom_solution})
            return resultat
        try:
            time0=time.time() 
            fonction_etudiant=getattr(module_etu,self.nom_solution)
            soletu_vi=test_fonction.test_fonction(fonction_etudiant,self.entrees_visibles)
            soletu_invi=test_fonction.test_fonction(fonction_etudiant,self.entrees_invisibles)
            temps=time.time()-time0
            resultat.temps(temps)
        except test_fonction.Erreur as e:
            resultat.invalide({"exception":e.exception, "erreur":e.erreur})
            return resultat
        zip_visibles=zip(self.solutions_visibles,soletu_vi)
        zip_invisibles=zip(self.solutions_invisibles,soletu_invi)
        resultat.add_mauvaises_sorties([((str(ent_ens),str(sor_etu),str(sor_ens))) for ((ent_ens,sor_ens),(ent_etu,sor_etu)) in zip_visibles if sor_ens!=sor_etu])
        resultat.add_mauvaises_sorties([((" cachée"," quelque chose",("autre chose",""))) for ((ent_ens,sor_ens),(ent_etu,sor_etu)) in zip_invisibles if sor_ens!=sor_etu])
        return resultat

