from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from appli.forms import *
from appli.models import *
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from appli.moduleExercice.Exercice import *
import time
from datetime import datetime as dtm
import datetime
import tempfile
from appli.moduleExercice import *
from appli.moduleExercice import module_ens
from appli.moduleExercice.Exercice import *
from django.contrib.auth.decorators import permission_required
import os
from EasyPython import settings

# Create your views here.

def test(self):
	titre="ceci est un test pour la page de code URL"
	invalide=self.session.get('reussit')
	return render(self, 'appli/test.html', locals())

@login_required
def contact(request):
	user2 = (request.user.username.split(".")[1])
	user2=user2[0].upper()+user2[1:]
	obt = help(user2)
	titre="ceci est un test pour la page de code URL"
	return render(request, 'appli/contact.html', locals())

@login_required
def accueil(self):
	titre="Bienvenue sur Easy Python"
	devellopeur="Devellope par Coudercy Pierre"
	user2 = (self.user.username.split(".")[1])
	user2=user2[0].upper()+user2[1:]
	obt = beta(user2)
	data = get_last_exo()
	art = get_arts()
	return render(self, 'appli/accueil.html',locals())

#=======================================================================
#Pages concernant les recherches d'exercice et de parcours
@login_required
def rechercher_exercice(request):
	tag=[x.nom for x in Tag.objects.all()]
	tag.append("Tous")
	print(tag)
	listTag=[]
	for elem in tag:
		listTag.append((elem,elem))
	if request.method == 'POST':
		print ("POST")
		form = RechercheExerciceForm(request.POST)
		form.fields['select_tags'].choices=listTag
		if form.is_valid():
			print ("valid")
			nom_exo = form.cleaned_data['nom_exercice']
			print("54: "+nom_exo)
			tags=form.cleaned_data['select_tags']
			print("tags: "+str(tags))
			envoi = True
			rep=[]
			data=recherche(nom_exo,tags)
			try:
				print ("try")
				for d in data:
					t=get_tag_exercice(d.titre)
					print (str(t))
					rep.append({"titre":d.titre,"enonce":d.enonce,"id":d.id,"tags":t})
				print ("Contenu de rep : \n  " + str(rep) + "\n Fin contenu de rep \n  ")
			except:
				print ("except")
				data = get_exos()
				rep=[]
				for d in data:
					t=get_tag_exercice(d.titre)
					rep.append({"titre":d.titre,"enonce":d.enonce,"id":d.id,"tags":t})
	else:
		print ("else")
		form = RechercheExerciceForm()
		form.fields['select_tags'].choices=listTag
		data = get_exos()
		rep=[]
		for d in data:
			t=get_tag_exercice(d.titre)
			print(t)
			rep.append({"titre":d.titre,"enonce":d.enonce,"id":d.id,"tags":t})
			#print (rep)
	return render(request, 'appli/recherche_exercice.html', locals())

@login_required
def rechercher_parcours(request):
	etapes=[]
	liste_etapes=[]
	nom_exercices=[]
	nb_etape=0
	if request.method == 'POST':
		print ("POST")
		form = RechercheExerciceForm(request.POST)
		if form.is_valid():
			print ("valid")
			nom_lot = form.cleaned_data['nom_exercice']
			print("111: "+nom_lot)
			envoi = True
			rep=[]
			try:
				print ("118 try")
				for d in data:
					etapes = get_ordreEtapeParLot(d.titre)
					if len(etapes)<2:
						nb_etape = "("+str(len(etapes))+" étape)"
					else:
						nb_etape = "("+str(len(etapes))+" étapes)"
					for e in etapes:

						print(e.etapes.all()[0].etape.titre)
					rep.append({"titre":d.titre, "nb_etape":nb_etape})
			except:
				print ("except")
				data = get_lots()
				rep=[]
				for d in data:
					etapes = get_ordreEtapeParLot(d.titre)
					if len(etapes)<2:
						nb_etape = "("+str(len(etapes))+" étape)"
					else:
						nb_etape = "("+str(len(etapes))+" étapes)"
					rep.append({"titre":d.titre, "nb_etape":nb_etape})
	else:
		print ("else")
		form = RechercheExerciceForm()
		data = get_lots()
		print (str(data))
		rep=[]
		for d in data:
			liste_etapes=[]
			etapes = get_ordreEtapeParLot(d.titre)
			if len(etapes)<2:
				nb_etape = "("+str(len(etapes))+" étape)"
			else:
				nb_etape = "("+str(len(etapes))+" étapes)"
			for e in etapes:
				titre_etape=e.etape.titre
				exo_etape = e.etape.exercices.all()
				nom_exercices=[]
				for e in exo_etape:
					nom_exercices.append(e.titre)
				liste_etapes.append({'titre_etape' : titre_etape, 'a' : nom_exercices})
			titre_etape=""

			rep.append({"titre":d.titre, "nb_etape":nb_etape,"detail":liste_etapes})
	return render(request, 'appli/recherche_parcours.html', locals())

#=======================================================================
#Pour les profils élèves et profs
@login_required
def profil(request):


	#==récupération de tous les exos
	lesExos = get_exos();

	#==Initialisation de l'exo que le user va selectionner dans la liste
	exercice = None

	#==récupération du user courant
	user2 = (request.user.username.split(".")[1])
	user2=user2[0].upper()+user2[1:]



	#==partie badges
	verif_badge_user(user2)

	#~ dbadges=get_badges().reverse()
	dbadges=get_badges_Utilisateur(user2)
	tailleBadgesUtil=len(dbadges)
	badges=get_badges()
	tailleBadges=len(badges)

	print("debuggage")
	print(dbadges)
	print("debuggage")
	lastbadges=[]


	#==récupération des 7 derniers jours
	dates=[]
	for i in range(-1,7):
		dt = datetime.datetime.now() - datetime.timedelta(days=i)  # on prend le jour d'auj - le ième
		d_truncated = datetime.date(dt.year, dt.month, dt.day) # on tronque cette date pour enlever les HH:mm:ss
		test = d_truncated
		dates.append(test)
	dates.reverse() 		# on inverse la liste pour obtenir les dates chronologique ( de gauche à droite)


	#récupération des données des liste déroulantes
	if request.method == 'POST':
		form = AffinerGraphique(request.POST)
		if form.is_valid():
			valeurListeExo = form.cleaned_data['nom_exo']		# recup de la premiere liste déroulante
			categorie = form.cleaned_data['categorie']			# recup de la 2eme liste déroulante
			if valeurListeExo == "-- Tous les exercices --":
				exercice = None
			else:
				exercice = valeurListeExo						# l'exercice plus haut initialisé à None
			print(valeurListeExo)
			print(categorie)


	# pour graphique du nombre de tentatives total
	# récupération du nombre de tentatives faites par le user courant sur un exercice donné
	tentatives=[]
	tentatives_final=dict()						# dictionnaire qui va contenir { date: nombre tentatives }
	for elem in dates:
		tentatives=(nb_tentative(user2,exercice))				#pour chacun des 7 derniers jours, on prend chaque tentative faite par le user sur l'exercice
		for elem2 in tentatives:								# on parcours la liste
			laDate = elem2.date
			laDate = datetime.date(laDate.year, laDate.month, laDate.day)	# on tronque la date
			print(elem)
			print("ladate:")
			print(laDate)
			print("\n")
			if (laDate==elem):										# si la date de l'exercice récupéré correspond à la date que l'on teste
				if(elem not in tentatives_final):
					tentatives_final[elem]=1					# initialisation dans le dictionnaire
				else:
					tentatives_final[elem]+=1					# on rajoute une occurence de tentative

	listeInd=[]
	for i in range(7):
		listeInd.append(0)							# préparation de la liste qui va servir au graphique

	for i in range(len(listeInd)):
		if dates[i] in tentatives_final:			# on teste chacun de nos 7 derniers jour s'il est en liaison avec une ou plusieurs tentatives
			listeInd[i]=tentatives_final[dates[i]]	# si oui, on indique le nombre de tentatives grâce au dictionnaire



	# pour graphique du nombre de tentatives réussites
	# récupération du nombre de tentatives réussites faites par le user courant sur un exercice donné
	tentatives2=[]
	tentatives_final2=dict()
	for elem in dates:
		tentatives2=(nb_tentative_reussite(user2,exercice))
		for elem2 in tentatives2:
			laDate = elem2.date
			laDate = datetime.date(laDate.year, laDate.month, laDate.day)
			if (laDate==elem):
				if(elem not in tentatives_final2):
					tentatives_final2[elem]=1
				else:
					tentatives_final2[elem]+=1
	listeInd2=[]
	for i in range(7):
		listeInd2.append(0)

	for i in range(len(listeInd)):
		if dates[i] in tentatives_final2:
			listeInd2[i]=tentatives_final2[dates[i]]


	# partie badge
	cpt=0
	for d in dbadges.reverse():
		if (cpt<=5):
			lastbadges=lastbadges+[d]
		cpt=cpt+1
	tailleLastB=len(lastbadges)


	titre="Profil"
	devellopeur="Devellope par Aurelien Branchoux"

	print(listeInd)
	return render(request, 'appli/profil.html',locals())

@login_required
def profilProf(request):

	#==récupération de tous les exos
	lesExos = get_exos();

	#==Initialisation de l'exo et du groupe que le user va selectionner dans la liste
	exercice = None
	groupe = None

	#==récupération du user courant
	user2 = (request.user.username.split(".")[1])
	user2=user2[0].upper()+user2[1:]



	#==partie badges
	verif_badge_user(user2)

	dbadges=get_badges_Utilisateur(user2)
	tailleBadgesUtil=len(dbadges)
	badges=get_badges()
	tailleBadges=len(badges)

	print("debuggage")
	print(dbadges)
	print("debuggage")
	lastbadges=[]
	listeGroupe=["1A", "1A1", "1A11", "1A12","1A2", "1A21", "1A22","1A3", "1A31", "1A32","1A4", "1A41", "1A42", user2 ]
	dicoGroupe={"1A" : 1, "1A1" : 11, "1A11" : 111, "1A12" : 112, "1A2" : 12, "1A21" : 121, "1A22" : 122, "1A3" : 13, "1A31" : 131, "1A32" : 132, "1A4" : 14, "1A41" : 141, "1A42" : 142, user2 : user2 }


	#==récupération des 7 derniers jours
	dates=[]
	for i in range(-1,7):
		dt = datetime.datetime.now() - datetime.timedelta(days=i)  # on prend le jour d'auj - le ième
		d_truncated = datetime.date(dt.year, dt.month, dt.day) # on tronque cette date pour enlever les HH:mm:ss
		test = d_truncated
		dates.append(test)
	dates.reverse() 		# on inverse la liste pour obtenir les dates chronologique ( de gauche à droite)


	#récupération des données des liste déroulantes
	if request.method == 'POST':
		form = AffinerGraphiqueProf(request.POST)
		if form.is_valid():
			valeurListeExo = form.cleaned_data['nom_exo']		# recup de la premiere liste déroulante
			categorie = form.cleaned_data['categorie']			# recup de la 2eme liste déroulante
			valeurListeGroupe = form.cleaned_data['groupe']				# recup du groupe
			groupe = valeurListeGroupe							# le groupe plus haut initialisé à None

			if valeurListeExo != "-- Tous les exercices --":
				exercice = valeurListeExo						# l'exercice plus haut initialisé à None

			print(exercice)
			print(categorie)
			print(groupe)


#------------------------------ pour graphique du nombre de tentatives total----------------------------------------
	# récupération du nombre de tentatives faites par le groupe sur un exercice donné
	tentatives=[]
	tentatives_final=dict()						# dictionnaire qui va contenir { date: nombre tentatives }
	for elem in dates:
		#~ tentatives=(nb_tentative(user2,exercice))				#pour chacun des 7 derniers jours, on prend chaque tentative faite par le user sur l'exercice


#===================DECOMMENTER QUAND FONCTION EXISTE!!!!===============================#
		if groupe=="-- Tous les groupes --":
			groupe=None
		if groupe == user2:
			tentatives=(nb_tentative(user2,exercice))
		else:
			if groupe!=None:
				tentatives=(nb_tentatives_groupes(get_utils(dicoGroupe[groupe]),exercice))
			else:
				tentatives=((nb_tentatives_groupes(get_utils(1),exercice)))
#=============================================================================================#
		print("==============================================================")
		print(tentatives)
		for elem2 in tentatives:								# on parcours la liste
			laDate = elem2.date
			laDate = datetime.date(laDate.year, laDate.month, laDate.day)	# on tronque la date
			print(elem)
			print("ladate:")
			print(laDate)
			print("\n")
			if (laDate==elem):										# si la date de l'exercice récupéré correspond à la date que l'on teste
				if(elem not in tentatives_final):
					tentatives_final[elem]=1					# initialisation dans le dictionnaire
				else:
					tentatives_final[elem]+=1					# on rajoute une occurence de tentative

	listeInd=[]
	for i in range(7):
		listeInd.append(0)							# préparation de la liste qui va servir au graphique

	for i in range(len(listeInd)):
		if dates[i] in tentatives_final:			# on teste chacun de nos 7 derniers jour s'il est en liaison avec une ou plusieurs tentatives
			listeInd[i]=tentatives_final[dates[i]]	# si oui, on indique le nombre de tentatives grâce au dictionnaire

#-------------------------- pour graphique du nombre de tentatives réussites-----------------------------------------
	# récupération du nombre de tentatives réussites faites par le groupe sur un exercice donné
	tentatives2=[]
	tentatives_final2=dict()
	for elem in dates:
		#~ tentatives2=(nb_tentative_reussite(user2,exercice))


#===================DECOMMENTER QUAND FONCTION EXISTE!!!!===============================#
		if groupe=="-- Tous les groupes --":
			groupe=None
		if groupe == user2:
			tentatives2=(nb_tentative_reussite(user2,exercice))
		else:
			if groupe!=None:
				tentatives2=(nb_tentative_reussite_groupe(get_utils(dicoGroupe[groupe]),exercice))
			else:
				tentatives2=((nb_tentative_reussite_groupe(get_utils(1),exercice)))

#=============================================================================================#


		for elem2 in tentatives2:
			laDate = elem2.date
			laDate = datetime.date(laDate.year, laDate.month, laDate.day)
			if (laDate==elem):
				if(elem not in tentatives_final2):
					tentatives_final2[elem]=1
				else:
					tentatives_final2[elem]+=1
	listeInd2=[]
	for i in range(7):
		listeInd2.append(0)

	for i in range(len(listeInd)):
		if dates[i] in tentatives_final2:
			listeInd2[i]=tentatives_final2[dates[i]]

	# partie badge
	cpt=0
	for d in dbadges.reverse():
		if (cpt<=5):
			lastbadges=lastbadges+[d]
		cpt=cpt+1
	tailleLastB=len(lastbadges)


	titre="Profil"
	devellopeur="Devellope par Aurelien Branchoux"

	print(listeInd)
	return render(request, 'appli/profilProf.html',locals())

#=======================================================================	
#Pour creer ce qui est necessaire aux exercices	
@login_required
@permission_required('appli.add_exercice')
def creer_exercice(request):
	string2=ecrireEnteteForm()
	print("string2="+string2)
	form = CreerExerciceForm(initial={'codes': string2})
	tags=get_tags()
	good=True
	if request.method == 'POST':
		form = CreerExerciceForm(request.POST)
		if form.is_valid():
			nom_exercice = form.cleaned_data['nom_exercice']
			nouveaux_tags = form.cleaned_data['nouveaux_tags']
			print ("366: nom_ex= "+nom_exercice)
			print ("384: tag= "+nouveaux_tags)
			list_Tag=nouveaux_tags.split("/")
			for elem in list_Tag:
				aj_tag(elem.lower())
			nom_exercice=nom_exercice.split(" ")
			cpt=0
			print ("369: nom_ex= "+str(nom_exercice)+" de taille "+str(len(nom_exercice)))
			l2=[]
			cpt2=0
			for elem in nom_exercice:
				if elem=='':
					print("test")
					cpt2+=1
					if cpt2<2:
						l2.append('_')
					else:
						pass
				else:
					l2.append("_")
					l2.append(elem.lower())
					cpt2=0
			while l2[0]=='_':
				l2.pop(0)
			if l2[0]=='':
				l2.pop(0)
			if l2[-1]=='':
				l2.pop(-1)
			if l2[-1]=="_":
				l2.pop(-1)
			nom_exercice="".join(l2)
			print (nom_exercice)
			enonce = form.cleaned_data['enonce']
			codes = form.cleaned_data['codes']
			erreurCode=False
			erreurDef=False
			verifCode = codes.replace("\r\n", "").replace(" ","").replace("	","")
			try:
				if verifCode.split("entrees_invisibles=[(")[1].split("),(")[0]=="" or verifCode.split("entrees_visibles=[(")[1].split("),(")[0]=="":
					erreurCode=True
			except:
					erreurCode=True
			try:
				if verifCode.split("@solutiondef")[1]=="":
					erreurDef=True
			except:
					erreurDef=True
			if erreurCode or erreurDef:
				return render(request, 'appli/creation_exercice.html', locals())
			try:
				user = (request.user.username.split("."))
				user = user[0][0].upper()+user[0][1:]+" "+user[1][0].upper()+user[1][1:]
				aj_exo(nom_exercice, user, enonce)						#ajouter l'utilisateur connecté (à améliorer)
				exoEns(nom_exercice, codes)								#creer fichier ens
				for elem in list_Tag:
					get_exo_titre(nom_exercice).tags.add(get_tag_par_nom(elem.lower()))
				return render(request, 'appli/accueil.html', locals())
			except:
				good=False
				return render(request, 'appli/creation_exercice.html', locals())
			#~ return render(request, 'appli/accueil.html', locals())
	return render(request, 'appli/creation_exercice.html', locals())

@login_required
#~ @permission_required('appli.add_exercice')
def creer_parcours(request):
	if request.method == 'POST':
		form = CreerParcours(request.POST)
		if form.is_valid():
			nom_parcours = form.cleaned_data['nom_parcours']
			nom_parcours=nom_parcours.split(" ")
			cpt=0
			l2=[]
			cpt2=0
			for elem in nom_parcours:
				if elem=='':
					print("test")
					cpt2+=1
					if cpt2<2:
						l2.append('_')
					else:
						pass
				else:
					l2.append("_")
					l2.append(elem.lower())
					cpt2=0
			while l2[0]=='_':
				l2.pop(0)
			if l2[0]=='':
				l2.pop(0)
			if l2[-1]=='':
				l2.pop(-1)
			if l2[-1]=="_":
				l2.pop(-1)
			nom_parcours="".join(l2)
			print (nom_parcours)
			try:
				aj_lot(nom_parcours)
				return render(request, 'appli/accueil.html', locals())
			except:
				print('except de creer_parcours')
				data = get_exos()
				rep=[]
				for d in data:
					t=get_tag_exercice(d.titre)
					print(t)
					rep.append({"titre":d.titre,"enonce":d.enonce,"id":d.id,"tags":t})
				return render(request, 'appli/creation_parcours.html', locals())
	else:
		print('else de creer_parcours')
		data = get_exos()
		rep=[]
		for d in data:
			t=get_tag_exercice(d.titre)
			print(t)
			rep.append({"titre":d.titre,"enonce":d.enonce,"id":d.id,"tags":t})
		return render(request, 'appli/creation_parcours.html', locals())
	return render(request, 'appli/creation_parcours.html', locals())

@login_required
def exercice(request,id_exo):
	if not get_exo(id_exo):
		raise Http404
	exo = get_exo(id_exo)
	#On utilise une fct pour recuperer la structure de la fct attendue
	solutionAttendue=ExercicePython("appli.moduleExercice."+exo.titre)
	if solutionAttendue.nom_solution==None:
		raise Http404
	string = ecrireEnteteCode(solutionAttendue.nom_solution, solutionAttendue.arguments)
	if request.session.get('code')!=None and solutionAttendue.nom_solution==request.session.get('code').split("def ")[1].split("(")[0]:
		form=ProgramForm(initial={'message':request.session.get('code')})
	else:
		form=ProgramForm(initial={'message': string})
	#affichage des erreurs pour les mauvais resultat
	mauvais_res = request.session.get('mauvais_resultat')
	if mauvais_res!=None:
		error_clear = "Erreur :"
		for mauvais in mauvais_res:
			for m in mauvais:
				if m[0]!=" cachée":
					if "[" in m[0] or "]" in m[0]:
						erreur=m[0].replace("(","").replace(")","")
					else:
						erreur=m[0].replace("(","").replace(","," ").replace(")","")
					erreur=erreur[0:(len(erreur)-1)]
					error_clear+=" la fonction devrait renvoyer "+erreur+" et renvoie "+m[1]+","
		error_clear=error_clear[0:(len(error_clear)-1)]+"."
	#l'affichage pour les syntax invalid est directement geree dans le template voir le block script en fin de page
	invalide = request.session.get('invalide')
	reussit  = request.session.get('reussit')
	request.session['mauvais_resultat']=None
	request.session['invalide']=None
	request.session['reussit'] = None
	aides = get_aides()
	titre = exo.titre[0].upper()+exo.titre[1:]
	enonce=exo.enonce
	cle=""
	definition=""
	for a in aides:
		if a.cle in enonce:
			cle=cle+a.cle
			definition=definition+a.definition
	enonce=enonce.split()
	return render(request, 'appli/exercice.html', locals())

@login_required
def aide(request):
	user2 = (request.user.username.split(".")[1])
	user2 = user2[0].upper()+user2[1:]
	obt = cultive(user2)
	titre = "Aide"
	dataAide=get_aides()
	return render(request, 'appli/aide.html', locals())

@login_required
def testerExercice(request, exercice):
	if request.method == 'POST':
			pf = ProgramForm(request.POST)
			user = (request.user.username.split(".")[1])
			user = user[0].upper()+user[1:]
			if pf.is_valid():
			#~ print("457: Le formulaire est valide")
				exo = get_exo(exercice)
				rep=testerSolution(pf.cleaned_data['message'], exo.titre)#On stocke dans rep tout ce qu'on a besoin de savoir pour indiquer le résultat d'un exercice
			#if rep[0]!=[[],[]]: #On a un mauvais résultat
			if not rep[1]:
				request.session['mauvais_resultat']=rep[0]
				request.session['invalide']=rep[3]
				request.session['code']=pf.cleaned_data['message']
			else:
				request.session['reussit']="Bravo, vous avez réussi votre exercice"
			if (rep[2]): 												#Si on obtient la badge démolisseur
				demolisseur(user)
			#On met en place l'enregistrement de la tentative
			a=request.user.username  									#Pour cela on récupère l'utilisateur,
			b=get_exo(exercice).titre  									#le titre de l'exercice
			user = request.user.username.split(".")[1]
			nom = user[0].upper()+user[1:]
			ten=aj_tentative_2(nom,exercice,rep[1])[1] 					#On ajoute la tentative
			verif_badge_user(user)										#On vérifie pour les badges indiquant les réussites
			#~ print ("472: tentative= ",ten)
			#valide en permanence
			return redirect('appli.views.exercice', id_exo=exercice)
	else:
			pf = ProgramForm()
			return render(request, 'appli/accueil.html',locals())

def connexion(request):
	error= False
	if request.method == "POST":
		form = ConnexionForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username = username, password = password)
			if user:
				login(request, user)
				suivant=request.REQUEST.get("next","")
				if settings.TEMPLATE_DEBUG:
					user = (request.user.username.split(".")[1])
					user = user[0].upper()+user[1:]
					beta(user)
				if suivant:
					return redirect(suivant)
				else:
					return redirect('appli.views.accueil')
			else:
				error = True
	else:
		form = ConnexionForm()
	return render(request,"appli/connexion.html", locals())

def deconnexion(request):
	logout(request)
	return redirect(reverse(connexion))

@login_required
def badges(request):

	# récupération du user courant
	user2 = (request.user.username.split(".")[1])
	user2=user2[0].upper()+user2[1:]

	verif_badge_user(user2)

	#~ dbadges=get_badges().reverse()
	dbadges=get_badges_Utilisateur(user2)
	tailleBadgesUtil=len(dbadges)
	badges=get_badges()
	tailleBadges=len(badges)
	aux=False
	for b in badges:
		for b2 in dbadges:
			if b == b2:
				aux=True
		if aux==False:
				b.image="NA"+b.image
		aux=False

	titre="Badges"
	devellopeur="Devellope par Aurelien Branchoux"
	return render(request, 'appli/badges.html',locals())

@login_required
@permission_required('appli.add_aide')
def creer_aide(request):
	good=True															#Pour savoir si une aide est présente ou non
	nom=""																#Si l'aide existe on utilise la variable pour le template
	if request.method == 'POST':
		#~ print ("579 On rentre dans POST ")
		form = CreerAideForm(request.POST)
		if form.is_valid():
			nom_aide = form.cleaned_data['nom_aide']
			describ = form.cleaned_data['describ']
			#~ print ("601: nom_aide= "+nom_aide)
			#~ print ("602: descr= "+describ)
			nom_aide=nom_aide.split(" ")
			cpt=0
			#~ print ("605: nom_aide= "+str(nom_aide)+" de taille "+str(len(nom_aide)))
			cpt2=0
			l2=[]
			for elem in nom_aide:
				if elem=='':
					print("test")
					cpt2+=1
					if cpt2<2:
						l2.append('_')
					else:
						pass
				else:
					l2.append("_")
					l2.append(elem.lower())
					cpt2=0
			while l2[0]=='_':
				l2.pop(0)
			if l2[0]=='':
				l2.pop(0)
			if l2[-1]=='':
				l2.pop(-1)
			if l2[-1]=="_":
				l2.pop(-1)
			nom_aide="".join(l2)
			print (nom_aide)
			try:
				ok = aj_aide(nom_aide, describ)							#On ajoute l'aide et on récupère le booleen indiquant si c'est bon ou pas
				#~ print("633 ok= "+str(ok))
				if not ok:
					good=False
					return render(request, 'appli/creation_aide.html', locals())
				return render(request, 'appli/accueil.html', locals())
			except:
				good=False
				return render(request, 'appli/creation_aide.html', locals())

	form = CreerAideForm()
	return render(request, 'appli/creation_aide.html', locals())
