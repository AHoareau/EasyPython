from django.core.management.base import BaseCommand
import yaml
from django.db import models, migrations
from appli.models import *
from django.contrib.auth.models import User
import time

class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Met a jour la bd pour les tentatives a partir d un fichier yaml'
	
	def handle(self, *args, **options):
		tentatives=yaml.load(open('./appli/Yaml/tentative.yml'))
	
		#import des modeles.
		from appli.models import Tentative
	
		# On crée les tentatives.
		i=0
		for b in tentatives:
			user=get_utilisateur(b['nom'])
			exo=get_exo_titre(b['titre'])
			new_tentative=Tentative(nom=user,
				titre=exo,
				reussi=b['reussi'])
			time.sleep(2)

				
			new_tentative.save()
