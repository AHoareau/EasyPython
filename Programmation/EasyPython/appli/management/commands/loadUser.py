from django.core.management.base import BaseCommand
from django.contrib.auth.models import Permission
import yaml
from django.db import models, migrations
from django.contrib.auth.models import User

class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Met a jour la bd pour les utilisateurs a partir d un fichier yaml'
	
	def handle(self, *args, **options):
		users=yaml.load(open('./appli/Yaml/utilisateur.yml'))
	
		#import des modeles.
		from appli.models import Utilisateur
	
		# On crée les utilisateurs.
		i=0
		for b in users:
                        username = b['prenom'].lower()+"."+b['nom'].lower()
                        mail     = username+"@etu.univ-orleans.fr"
                        user     = User.objects.create_user(username,mail,b['password'])
                        user.first_name = b['prenom']
                        user.last_name = b['nom']
                        if b['type'] == 1:
                                user.is_staff = True
                                content=Permission.objects.get(codename="add_exercice")
                                user.user_permissions.add(content)
                        elif b['type'] == 2:
                                user.is_superuser = True
                        user.save()
                        new_user = Utilisateur(user = user, groupe = b['groupe'])
                        new_user.save()   
