from django.core.management.base import BaseCommand
from django.contrib.auth.models import Permission
import yaml
from django.db import models, migrations
from django.contrib.auth.models import User

class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Met a jour la bd pour les parcours a partir d un fichier yaml'
	
	def handle(self, *args, **options):
		#~ users=yaml.load(open('./appli/Yaml/utilisateur.yml'))
	
		#import des modeles.
		from appli.models import Lot, Etape, aj_etape, aj_ordre_etape, get_exo_titre
	
		# On crée les lots.
		new_lot=Lot(titre="Lot1")
		new_lot.save()
		new_lot=Lot(titre="Lot2")
		new_lot.save()
		
		# On crée les étapes.
		aj_etape("Etape1", 1)
		aj_etape("Etape2", 1)
		aj_etape("Etape3", 1)
		aj_etape("Etape4", 1)
		
		# On ajoute des exercices aux etapes.
		Etape.objects.filter(titre="Etape1")[0].exercices.add(get_exo_titre("ma_première_fonction"))
		Etape.objects.filter(titre="Etape1")[0].exercices.add(get_exo_titre("condition_partie_1"))
		Etape.objects.filter(titre="Etape2")[0].exercices.add(get_exo_titre("trier_un_tableau"))
		
		#On ajoute les etapes dans l'ordre aux lots 
		Lot.objects.filter(titre="Lot1")[0].etapes.add(aj_ordre_etape(Etape.objects.filter(titre="Etape1"),1))
		Lot.objects.filter(titre="Lot1")[0].etapes.add(aj_ordre_etape(Etape.objects.filter(titre="Etape2"),2))
