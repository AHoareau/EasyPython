from django.core.management.base import BaseCommand
import yaml
from django.db import models, migrations

class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Met a jour la bd pour les badges a partir d un fichier yaml'
	
	def handle(self, *args, **options):
		badges=yaml.load(open('./appli/Yaml/badge.yml'))
	
		#import des modeles.
		from appli.models import Badge
	
		# On crée les utilisateurs.
		i=0
		for b in badges:
			new_badge=Badge(nom=b['nom'],
				image=b['image'],
				description=b['description'])
				
			new_badge.save()
