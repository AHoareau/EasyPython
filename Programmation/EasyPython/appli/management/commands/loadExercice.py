from django.core.management.base import BaseCommand
import yaml
from django.db import models, migrations
from appli.models import get_tag_par_nom

class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Met a jour la bd pour les exercices a partir d un fichier yaml'
	
	def handle(self, *args, **options):
		exercices=yaml.load(open('./appli/Yaml/exercice.yml'))
	
		#import des modeles.
		from appli.models import Exercice
	
		# On crée les vins.
		i=0
		for b in exercices:
			new_exo=Exercice(titre=b['titre'],
				auteur=b['auteur'],
				enonce=b['enonce'])
			new_exo.save()
			
    
		Exercice.objects.all()[0].tags.add(get_tag_par_nom("boucle")) 	#=> pour ajouter un Tag
		Exercice.objects.all()[2].tags.add(get_tag_par_nom("expression reguliere"))

